<?php

/**
* Plugin Name: GNUSocial Autoquip
* Plugin URI: https://elbinario.net
* Description: This plugin send quips on new post published
* Version: 1.0.0
* Author: foo@gnusocial.net
* Author URI: https://elbinario.net/author/foo
* License: GPLv3
*/

function menu_page(){
	add_menu_page('settings','GNUSocial Autoquip', 'manage_options', 'Settings GNUSocial', 'settings_page');
}

function settings_page(){
?>
	<br>
	<br>
	<br>
    <center>
	<b>GNUSocial Account Settings</b>
	<br>
	<br>
	<table>
	<form action="options.php" method="post">
	<?php settings_fields('gnusocial_settings'); ?>
	<?php do_settings_sections('options.php'); ?>
    <tr><td>Username   </td><td><input type='text' name='username'     value='<?php echo esc_attr( get_option('username') ); ?>'></td></tr>  
    <tr><td>Password   </td><td><input type='password' name='password' value='<?php echo esc_attr( get_option('password') ); ?>'></td></tr>
    <tr><td>Social Node</td><td><input type='text' name='node'         value='<?php echo esc_attr( get_option('node') ); ?>'></td></tr>
    <tr></td></td></tr>
    <table><input type='submit' value='SAVE' onclick(this.form('submit'))></table>
    </form>
    </table>
    </center>
<?php  
}?>

<?php  

function register_mysettings() { 
  register_setting( 'gnusocial_settings', 'username' );
  register_setting( 'gnusocial_settings', 'password' );
  register_setting( 'gnusocial_settings', 'node' );
}


function public_quip($post_ID){
	
	$username = get_option('username');
	$password = get_option('password');
	$node = get_option('node');
	$curl_url = "https://".$node."/api/statuses/update.xml";
	$post_permalink =  get_permalink($post_ID);
	$post_title = get_the_title($post_ID);
	$quip = $post_title." ".$post_permalink;
	$postdada = array( "status" => $quip);
	$curl = curl_init($curl_url);
	curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => $curl_url,
	CURLOPT_USERAGENT => 'gnusocial autoquip wp plugin by foo@gnusocial.net',
	CURLOPT_POST => 1,
	CURLOPT_POSTFIELDS => $postdada,
	CURLOPT_USERPWD => $username.":".$password,
	));
	if(!wp_is_post_revision($post_ID)) {
		 curl_exec($curl);
	}	
	return $post_ID;
}


if(get_option('username') && get_option('password') && get_option('node')){
	add_action( 'publish_post', 'public_quip', 10, 2 );
}

add_action('admin_menu', 'menu_page');
add_action( 'admin_init', 'register_mysettings' );

?>
